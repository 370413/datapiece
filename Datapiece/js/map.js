"use strict";

var getDomain = function(d) {
	var output = [];
	var minimum, maximum, p;
	if (ind.min) {
		output[0] = ind.min;
	} else {
		for (p in d) {
			if (d[p] < minimum || !minimum) {
				minimum = d[p];
			}
		}
		output[0] = minimum;
	}
	if (ind.max) {
		output[1] = ind.max;
	} else {
		for (p in d) {
			if (d[p] > maximum || !maximum) {
				maximum = d[p];
			}
		}
		output[1] = maximum;
	}
	return output;
}


var dataset = ind["data"];


function useData() {

var year = map.year; //TOREMOVE




function mapLegend(value) {
	value = legend.scale(value) * 6;
	switch (true) {
		case value < 0:
			return 'no_data';
		case value < 1:
			return 'leg1';
		case value < 2:
			return 'leg2';
		case value < 3:
			return 'leg3';
		case value < 4:
			return 'leg4';
		case value < 5:
			return 'leg5';
		default:
			return 'leg6';
	}
}

function mapDataForMap() {
	var legendClass;
	var placeElement;
	for (var place in dataset[year]) {
		legendClass = mapLegend(dataset[year][place]);
		if (placeElement = document.getElementsByClassName(place)[0]) {
			try {
				placeElement.classList.add(legendClass);
			} finally {
				//console.log(place + " does not have data therefore can't be coloured");
			}	
		}	
	}
}

mapDataForMap();

window.removeEventListener("load", map.create, false);

}





var legend = { //TODO
	_absolute : false,
	smooth : false,
	visible : false,
	draw : function() {},
	show : function() {},
	hide : function() {},
	toggle : function() {},
	domain : [0, 1]
};

var map = {
	legend : legend,
	element : null,
	_year : 2010, //TODO
	_width : document.getElementById("map_div").offsetWidth * 0.98, // height is calculated based on width, so it's outside literal
	legendType : (
		function () { 
			switch(ind["dataKind"]) {
				case 0:
					return "seq";
				case 1:
					return "invseq";
				case 2:
					return "div";
				case 3:
					return "qual";
			}
		}
	)(),
	projection : function() {
		return d3.geo[ind["projection"]]()
	    		.center([ind["centerX"], ind["centerY"]])
	    		.scale(this.width*ind["scale"])
	    		.translate([this.width / 2, this.height / 2]);
	},
	create : function() {
	
		var element = this.element; // see visualise function
		
		// preparing DOM (svg)
		
		element = d3
			.select("#map_div")
			.append("svg")
			.attr("width", this.width)
			.attr("height", this.height)
			.attr("class", this.legendType)
			.attr("style", "display:none;");
		
		var path = d3
			.geo.path()
			.projection(this.projection());
			
		// function visualising data
		
		var visualise = function(error, world) {
			if (error) return console.error(error);
			var units = world.objects.subunits;
			element
				.selectAll(".place")
				.data(topojson.feature(world, units).features)
				.enter()
				.append("path")
				.attr(
					"class",
					 function(d) { 
					 	return "place " + d.id; 
					 }
				)
				.attr("d", path);
			element
				.append("path")
				.datum(
					topojson.mesh(
						world, 
						units, 
						function(a, b) { 
							return a === b ; 
						}
					)
				)
				.attr("d", path)
				.attr("class", "coastline"); // adds coastline
		}
		d3.json("js/world3.json", visualise);
		this.update();
		this.show();
	},
	update : useData, //TODO
	destroy : function() {
		this.element = null;
	},
	show : function() {
		this.element.style.display = "";
	},
	hide : function() {
		this.element.style.display = "none";
	},
	extremeValues : function() {
		var minimum, maximum;
		var d = dataset[map.year];
		for (var p in d) {
			if (d[p] < minimum || !minimum) {
				minimum = d[p];
			} else if (d[p] > maximum || !maximum) {
				maximum = d[p];
			}
		}
		return [minimum, maximum]
	}
};
Object.defineProperty(map, "year", {
	get: function() { console.log('a'); return this._year; },
	set: function(newYear) {
		this._year = newYear;
		this.update();
		console.log('b');
	}
});
Object.defineProperty(map, "width", {
	get: function() { return this._width; },
	set: function(newWidth) {
		this._width = newWidth;
		this._height = newWidth * 0.4;
	}
});
Object.defineProperty(map, "height", {
	get: function() { return this._width * 0.4; },
	set: function(newHeight) {
		this._width = newHeight * 2.5;
	}
});

Object.defineProperty(legend, "absolute", {
	get : function() { return this._absolute; },
	set : function(value) { 
		if (value) {
			this.domain[0] = 0;
		} else {
			this.domain[0] = map.extremeValues()[0];
		}
		this._absolute = value;
	}
});

legend.absolute = false;

Object.defineProperty(legend, "scale", {
	get: function() { return this._scale; },
	set: function(type) {
		this._scale = d3.scale[type]().domain(this.domain).range([0,1]);
	}
});

legend.scale = "linear";

 // map.end
 
 addEventListener("load", map.create, false);

//map.create();
