<?php


if ($_GET['i']) {
	$i = $_GET['i'];


$query = "SELECT indicators.name as name, indicators.description as description, indicators.min_value as min, max_value as max, log_scale+0 as logScale, custom_legend+0 as cl, cannot_group_labels+0 as cgl, data_kind+0 as dataKind, long_description as longDescription, periodicity, aggregation_method as aggregation, map+0 as canMap, chart+0 as canChart, indicators.table+0 as canTable, sources.name as sourceName, sources.website as sourceURL, sources.description as sourceDescription, units.name as unit, weighted+0 as weighted, categories.name as categoryName, categories.parent as parentCategory, categories.description as categoryDescription, regions.name as regionName, regions.projection as projection, regions.scale as scale, regions.center_x as centerX, regions.center_y as centerY, rotation FROM `datapiece`.`indicators` INNER JOIN `datapiece`.`units` ON unit = units_id INNER JOIN `datapiece`.`categories` ON category = categories_id INNER JOIN `datapiece`.`regions` on region = regions_id INNER JOIN `datapiece`.`sources` ON source = sources_id WHERE `indicator_id` = :ind";

try {
	$result = $pdo->prepare($query);
	$result->bindValue(':ind', $i, PDO::PARAM_INT);
	$result->execute();
	while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
		$success = true;
		$ind = $row;
	}
	$result->closeCursor();
	
} catch (PDOException $e) { echo $e->getMessage(); }

$query = "SELECT value, year, places.code as place FROM `datapiece`.`values` INNER JOIN `datapiece`.`places` on place = places_id WHERE indicator = :ind";

$data = [];

try {
	$result = $pdo->prepare($query);
	$result->bindValue(':ind', $i, PDO::PARAM_INT);
	$result->execute();
	while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
		if (!$data[$row['year']]) {
			$data[$row['year']] =[];
		}
		$data[$row['year']][$row['place']] = $row['value'];
	}
	
	$ind['data'] = $data;
	
	$json = json_encode($ind, JSON_NUMERIC_CHECK);
	
	echo "<script>var ind = " . $json . ";</script>";
	
} catch (PDOException $e) { echo $e->getMessage(); }




};

?>
