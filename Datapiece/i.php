<!DOCTYPE html>
<?php
	$home = "/Datapiece/";
	$username = 'root';
	$password = 'mysql';
	$database = 'datapiece';
	$servername = 'localhost';

	try
	{
		$pdo = new PDO("mysql:host=" . $servername . ";dbname=" . $database, $username, $password);
		//echo 'Połączenie nawiązane!';
	}
	catch(PDOException $e)
	{
		echo 'Połączenie nie mogło zostać utworzone: ' . $e->getMessage();
	}
	
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

?>
<html>
<head>
	<title><!-- INDICATOR --> - Datapiece</title>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	<link rel="stylesheet" href="style.css">
	<link rel="stylesheet" href="legend.css">
	<!--<link rel="stylesheet" media="screen and (min-width:1000px) and (orientation:landscape)" href="landscape_big.css">
	<link rel="stylesheet" media="screen and (min-width:1000px) and (orientation:portrait)" href="portrait_big.css">
	<link rel="stylesheet" media="screen and (max-width:1000px)" href="small.css">
	<link rel="stylesheet" href="bootstrap/bootstrap.css">-->
	<link rel="icon" type="image/png" href="images/favicon.png" />
	<?php include 'data.php'; ?>
	<script src="js/d3.js"></script>
	<script src="js/topojson.js"></script>
	<script src="js/geo.projection.js" charset="utf-8"></script>
	<script src="js/script.js"></script>
</head>

<body>


<div id="title_div" class="main main_with_content">

	
	<div id="title_label_div" class="left_part part">
		<h1>Data<span class="piece">piece</span> <span class="beta">BETA</span></h1>
	</div>
	<div id="title_ad_div" class="middle_part part ad">
		somethin' intrestin' 4 pancernych i psa
	</div>
	<div id="title_links_div" class="right_part part">
		<nav id="link_nav">
		<ul id="link_ul">
			<li>Polski</li>
			<li>English</li>
			<li>English</li>
			<li>English</li>
			<li>English</li>
		</ul>
		</nav>
	</div>
</div>
<div id="empty1_div" class="empty main">
	<div class="left_ad ad part">Ad1</div>
	<div class="right_ad ad part">Ad2</div>
</div>

<div class="content_wrapper">

<div id="indicator_label_div" class="main main_with_content">
	<div id="indicator_name_div" class="left_part part">
		<?php echo $ind['name']; ?>
	</div>
	<?php if($ind['unit']) { ?><div id="indicator_unit_div" class="right_part part">
		<?php echo $ind['unit']; ?>
	</div><?php } ?>
</div>
<div id="map_div" class="map main main_with_content main_vis">
		<!--Tu kod odpowiedzialny za wizualizację-->
		<script src="js/map2.js"></script>
</div>
<div id="table_div" class="table main main_with_content main_vis">
	<table>
		<tr>
			<th>Country</th>
		</tr>
	</table>
</div>
<div id="vis_settings_div" class="main main_with_content">
	<div id="map_settings_left" class="left_part part">
		<div id="vis_settings_minor" class="map vis_settings">
			<form>
			<ul>
				<li><label>
					Year
					<select name="year">
						<option value=2015>2015</option>
						<option value=2014>2014</option>
						<option value=2013>2013</option>
					</select>
				</label></li>
				<li><label>
					Scale
					<select name="scale">
						<option value="linear">linear</option>
						<option value="log">logarithmic</option>
						<option value="exp">exponential</option>
					</select>
				</label></li>
				<li><label><input type="checkbox" name="microstates" />Show microstates</label></li>
				<li><label><input type="checkbox" name="labels" />Show areas represented with text labels</label></li>
				<li><label><input type="checkbox" name="absolute_scale" />Start legend at 0</label></li>
				<li><label><input type="checkbox" name="smooth_scale" />Smooth legend</label></li>
				<li>For now let's pretend we'll only have a map and later we'll see</li>
			</ul>
			</form>
		</div>
		<div id="vis_settings_maior" class="vis_settings">
			<ul>
				<li>Map</li>
				<li>Table</li>
				<li>Graph</li>
			</ul>
		</div>
	</div>
	<div id="social_div" class="right_part part">
		<div id="download_options" class="social_divs invisible">
			Hey, why is that so much in centre?
			Why is one div above the other?
			<form>
				<label><input type="checkbox" checked />Be polite</label>
			</form>
		</div>
		<div id="social_buttons" class="social_divs">
		<ul>
			<li id="link">DIRECT LINK</li>
			<li id="like">LIKE</li>
			<li id="tweet">TWEET</li>
			<li id="reddit">REDDIT</li>
			<li id="download" onClick="this.parentNode.parentNode.parentNode.childNodes[1].classList.remove('invisible');">DOWNLOAD ↓</li>
		</ul>
		</div>
	</div>
</div>
<div id="info_div" class="main main_with_content">
	<div id="vis_description" class="left_part part">
		<p id="description_p" class="info_p">
			<?php echo $ind['description']; ?> <a>(More...)</a>
		</p>
	</div>
	<div id="source_div" class="right_part part">
		<p id="source_p" class="info_p">
			<span id="source_label" class="label">Source: </span><a href="http://worldbank.com">World Bank Databank 2013</a>
		</p>
	</div>
</div>

</div>

<div id="empty2" class="empty main">
	<div class="ad left_ad part">Ad1</div>
	<div class="ad right_ad part">Ad2</div>
</div>

<div class="content_wrapper">

<div id="search_div" class="main main_with_content">
	<div id="search_main_div" class="left_part part">
		<div id="search_input_div">
			<fieldset>
			<div id="search_input_inner_div">
				<input type="search" id="search_field" default="Population" />
				<button type="submit">SEARCH</button>
			</div>
			</fieldset>
		</div>
		
		<div id="search_query_div">
			<h2>Results for: <span id="search_query">boomerang</span></h2> (2015 items found)
		</div>
		
		<div id="search_results">
		<ul id="search_items_list">
			<li class="search_item">
			<div>
				<div class="head">
					<div class="indicator_title">
						<h3>Population</h3>
					</div>
					<div class="indicator_unit">
						% annually
					</div>					
				</div>
				<div class="body">
					<div class="indicator_meta">
						World Bank, 2015, NUTS 2 regions <a class="More" onClick="document.getElementsByClassName('indicator_description')[0].style.display = ''; document.getElementsByClassName('indicator_description')[0].classList.remove('invisible'); this.classList.add('invisible');">(More...)</a>
					</div>
					<div class="indicator_description invisible">
						GDP growth annually in % based on eye-wittnesses reports <a onClick="this.parentNode.classList.add('invisible'); this.parentNode.parentNode.childNodes[1].childNodes[1].classList.remove('invisible');">(Less...)</a>
					</div>
				</div>
			</div>
			</li>
		</ul>
		</div>
	</div>
	
	<div id="filter_div" class="right_part part">
		<fieldset>
			<span id="sort_label">Sort by</span>
			<select id="sort">
				<option value=1 selected>Popularity</option>
				<option value=2>Alphabetically</option>
			</select>
		</fieldset>
		<fieldset id="sources_filter">
			<legend>Sources</legend>
			<label><input type="checkbox" checked />All</label>
			<label>
				<input type="checkbox" checked />World Bank Databank
				<fieldset>
				<label><input type="checkbox" id="all" name="all" checked />All</label>
				<label><input type="checkbox" id="area" name="area" checked />Area</label>
				</fieldset>
			</label>
			<label><input type="checkbox" checked />Eurostat</label>
			<div id="sources_more">
			<label><input type="checkbox" checked />GUS</label>
			</div>
			<span id="sources_more_toggler">Show more...</span>
		</fieldset>
		<fieldset id="regions_filter">
			<legend>Regions</legend>
			<label><input type="checkbox" checked />World</label>
			<label><input type="checkbox" checked />Europe</label>
		</fieldset>
		<fieldset id="years_filter">
			<legend>Years</legend>
			<label><input type="radio" name="years"/>2005+</label>
			<label><input type="radio" checked name="years">2000+</label>
		</fieldset>
		<fieldset id="categories_filter">
			<legend>Categories</legend>
			<label><input type="checkbox" id="all" name="all" checked />All</label>
			<label><input type="checkbox" id="area" name="area" checked />Area</label>
		</fieldset>
	</div>
</div>

</div>


<div id="footer_div" class="footer main main_with_content">
	stopka :foot:
</div>



</body>
</html>
