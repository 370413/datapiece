Projekt strony prezentującej dane z internetu na mapie świata, 
tworzony przed pójściem na studia w 2015 roku. 
Technologie: PHP + Javascript (D3.js) for the website, Python + Bash for web data scrapping.
The photograph of Rio de Janeiro (c) my brother Łukasz