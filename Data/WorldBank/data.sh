#!/bin/bash

IFS='®'

## UWAGA! NAJPIERW: ZAMIENIĆ ' NA '' I /n NA <br /> W PLIKU Z DATA I ZAPISAĆ Z IFS=(R) ORAZ PUSTYMI KOMÓRKAMI=(C) I . JAKO SEPARATOREM DZIESIĘTNYM!

declare -A value
touch temp
i=1
declare -A c_dict

while read ckey name longName countryType subType sov capital currencyCode currency telephone code2 code_three number tld wbcode; do
	if [ -z $wbcode ]
	then
	c_dict[$name]=$ckey
	else
	c_dict[$wbcode]=$ckey
	fi
done < 'ISOS.csv'

declare -A i_dict

while read indkey ind long asource topic periodicity method concept relevance lae comments series_code; do
	i_dict[$series_code]=$indkey
done < 'SeriesMetadata_mod.csv'

inputfile="Data_mod.csv"
cat $inputfile | while read CountryName CountryCode SeriesName SeriesCode a1960 a1961 a1962 a1963 a1964 a1965 a1966 a1967 a1968 a1969 a1970 a1971 a1972 a1973 a1974 a1975 a1976 a1977 a1978 a1979 a1980 a1981 a1982 a1983 a1984 a1985 a1986 a1987 a1988 a1989 a1990 a1991 a1992 a1993 a1994 a1995 a1996 a1997 a1998 a1999 a2000 a2001 a2002 a2003 a2004 a2005 a2006 a2007 a2008 a2009 a2010 a2011 a2012 a2013; do
	c=${c_dict[$CountryCode]}
	indic=${i_dict[$SeriesCode]}
	value["1960"]=$a1960
	value["1961"]=$a1961
	value["1962"]=$a1962
	value["1963"]=$a1963
	value["1964"]=$a1964
	value["1965"]=$a1965
	value["1966"]=$a1966
	value["1967"]=$a1967
	value["1968"]=$a1968
	value["1969"]=$a1969
	value["1970"]=$a1970
	value["1971"]=$a1971
	value["1972"]=$a1972
	value["1973"]=$a1973
	value["1974"]=$a1974
	value["1975"]=$a1975
	value["1976"]=$a1976
	value["1977"]=$a1977
	value["1978"]=$a1978
	value["1979"]=$a1979
	value["1980"]=$a1980
	value["1981"]=$a1981
	value["1982"]=$a1982
	value["1983"]=$a1983
	value["1984"]=$a1984
	value["1985"]=$a1985
	value["1986"]=$a1986
	value["1987"]=$a1987
	value["1988"]=$a1988
	value["1989"]=$a1989
	value["1990"]=$a1990
	value["1991"]=$a1991
	value["1992"]=$a1992
	value["1993"]=$a1993
	value["1994"]=$a1994
	value["1995"]=$a1995
	value["1996"]=$a1996
	value["1997"]=$a1997
	value["1998"]=$a1998
	value["1999"]=$a1999
	value["2000"]=$a2000
	value["2001"]=$a2001
	value["2002"]=$a2002
	value["2003"]=$a2003
	value["2004"]=$a2004
	value["2005"]=$a2005
	value["2006"]=$a2006
	value["2007"]=$a2007
	value["2008"]=$a2008
	value["2009"]=$a2009
	value["2010"]=$a2010
	value["2011"]=$a2011
	value["2012"]=$a2012
	value["2013"]=$a2013
	statement="INSERT INTO \`datapiece\`.\`values\`(\`value\`,\`indicator\`,\`country\`,\`year\`) VALUES "
	for i in {1960..2013}
	do
		if [ '©' != ${value[$i]} ];
		then 
		statement=$statement"('${value[$i]}', '$indic', '$c', '$i'),"
		fi
	done
	statement=${statement%,}';'
	if [ "INSERT INTO \`datapiece\`.\`values\`(\`value\`,\`indicator\`,\`country\`,\`year\`) VALUES ;" != $statement ]
	then
	echo $statement
	fi
done | mysql -u root -pmysql
	echo $PIPESTATUS
