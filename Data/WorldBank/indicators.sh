#!/bin/bash

IFS='®'

## UWAGA! NAJPIERW: ZAMIENIĆ ' NA '' I /n NA <br /> W PLIKU Z SERIES-METADATA I ZAPISAĆ Z IFS=(R)!

inputfile="SeriesMetadata_mod.csv"
cat $inputfile | while read indcode ind long asource topic periodicity method concept relevance lae comments; do
	category=$((indcode+1))
	unit=`echo $ind | grep -o "(.*)"`
	
	if [ -z $unit ];
	then
	indicator=$ind
	else
	indicator=`echo $ind | sed -n "/(.*)/s/(.*)//p"`
	fi
	unit=${unit:1}
	unit=${unit%)}
	if [ -z $method ];
	then
	met=''
	else
	met="<p>Aggregation method: $method</p>"
	fi
	if [ -z $concept ];
	then
	concept2=''
	else
	concept2="<h5>Statistical concept and methodology</h5> <p>$concept/p>"
	fi
	if [ -z $relevance ];
	then
	rel2=''
	else
	rel2="<h5>Development relevance</h5> <p>$relevance/p>"
	fi
	if [ -z $lae ];
	then
	lae2=''
	else
	lae2="<h5>Limitations and exceptions</h5> <p>$lae/p>"
	fi
	if [ -z $comments ];
	then
	com2=''
	else
	com2="<h5>Limitations and exceptions</h5> <p>$comments/p>"
	fi	
	description="<p>$long</p> $met $concept2 $rel2 $lae2 $comments"
	echo "INSERT INTO \`datapiece\`.\`indicators\`(\`name\`,\`unit\`,\`description\`,\`source\`,\`category\`) VALUES ('$indicator', '$unit', '$description', '$asource', '$category');"
done | mysql -u root -pmysql
	


#while read country ccode ind indcode ﻿Country Name Country Code Series Name Series Code 1960  1961  1962  1963  1964  1965  1966  1967  1968  1969  1970  1971  1972  1973  1974  1975  1976  1977  1978  1979  1980  1981  1982  1983  1984  1985  1986  1987  1988  1989  1990  1991  1992  1993  1994  1995  1996  1997  1998  1999  2000  2001  2002  2003  2004  2005  2006  2007  2008  2009  2010  2011  2012  2013  do
	
