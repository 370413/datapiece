#!/usr/bin/python3
import psycopg2 as psc
import csv

filename = 'a9901a79-11f8-41c0-8f34-07143491e19a_Country - Metadata.csv'
f = open(filename)
r = csv.reader(f, delimiter=',', quotechar='"')
countries = [row for row in r]

connect_str = "dbname='datapiece_db' user='datapiece' host='localhost' password='POL191'"
conn = psc.connect(connect_str)
cur = conn.cursor()
cur.execute(
    "prepare myplan as insert into dp.places (PLACE_ID, TWO_LETTER_CODE, NAME, LONG_NAME, NOTES, THREE_LETTER_CODE) VALUES ($1, $2, $3, $4, $5, $6)")

i = 0

def repair(x):
    return x

header = countries[0]
print(header)
for country in countries[1:]:
    print(country)
    country_2_code = repair(country[28])
    country_3_code = repair(country[0])
    name = repair(country[-1])
    long_name = repair(country[1])
    
    notes_list = [repair(header[i]) + ": " + repair(country[i]) for i in range(2, 27) if repair(country[i]) != ""]
    notes = ""
    for n in notes_list:
        notes += n
        notes += "\n\n"
    
    cur.execute("execute myplan (%s, %s, %s, %s, %s, %s)", (i, country_2_code, name, long_name, notes, country_3_code))

    i += 1
    
    print(name)
rows = cur.fetchall()
print(rows)
